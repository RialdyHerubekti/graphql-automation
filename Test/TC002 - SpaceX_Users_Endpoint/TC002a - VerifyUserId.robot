***Settings***
Documentation    TC002a - VerifyUserId
Library          Collections
Library          RequestsLibrary
Library          JSONLibrary
Library          OperatingSystem
Library          DatabaseLibrary
Resource         ${EXECDIR}/Resource/UsersDetail/usersDetailUserIdisNullFalse.robot

*** Test Cases ***
Verify userid
    I want to check userid
    Verify The user id is not empty

