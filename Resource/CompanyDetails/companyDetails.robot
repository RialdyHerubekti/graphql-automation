***Settings***
Library     Collections
Library     RequestsLibrary
Library     JSONLibrary
Library     DatabaseLibrary
Library     OperatingSystem
Library     DateTime
Resource    ${EXECDIR}/Resource/BaseUrl/baseUrl.robot




*** Variables ***
${jsonFile}    ${EXECDIR}/Json/CompanyDetails/companyDetails.json


*** Keywords ***
I want to Check Company Profile
    ${file_data}=          Load Json from file       ${jsonFile}                      
    ${new_obj}=            Convert JSON To String    ${file_data}
    Create File            companyProfile.json       ${new_obj}
    Create Session         companyProfile            ${baseURL}
    ${headers}=            Create Dictionary         Content-Type=application/json
    ${new_file_data}=      Get Binary File           companyProfile.json
    ${response}=           POST On Session           companyProfile                   /graphql                               data=${new_file_data}    headers=${headers}
    ${companyProfile}=     Set Variable              ${response.json()}
    log                    ${companyProfile}
    ${companyCEO}=         Get Value From Json       ${companyProfile}                $.data.company.ceo
    ${companyCTO}=         Get Value From Json       ${companyProfile}                $.data.company.cto
    ${companyCOO}=         Get Value From Json       ${companyProfile}                $.data.company.coo
    ${companyAddress}=     Get Value From Json       ${companyProfile}                $.data.company.headquarters.address
    ${companyCity}=        Get Value From Json       ${companyProfile}                $.data.company.headquarters.city
    ${companyState}=       Get Value From Json       ${companyProfile}                $.data.company.headquarters.state
    Set Global Variable    ${companyCEO}
    Set Global Variable    ${companyCTO}
    Set Global Variable    ${companyCOO}
    Set Global Variable    ${companyAddress}
    Set Global Variable    ${companyCity}
    Set Global Variable    ${companyState}
Verify the Company Profile Result Should Not Empty
    Should Be Equal    ${companyCEO[0]}        Elon Musk          
    Should Be Equal    ${companyCTO[0]}        Elon Musk
    Should Be Equal    ${companyCOO[0]}        Gwynne Shotwell
    Should Be Equal    ${companyAddress[0]}    Rocket Road
    Should Be Equal    ${companyCity[0]}       Hawthorne
    Should Be Equal    ${companyState[0]}      California



