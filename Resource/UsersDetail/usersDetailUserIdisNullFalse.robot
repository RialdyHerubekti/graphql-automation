***Settings***
Library     Collections
Library     RequestsLibrary
Library     JSONLibrary
Library     DatabaseLibrary
Library     OperatingSystem
Library     DateTime
Resource    ${EXECDIR}/Resource/BaseUrl/baseUrl.robot




*** Variables ***
${jsonFile}    ${EXECDIR}/Json/UsersDetail/usersDetail.json
${false}       ${false}


*** Keywords ***
I want to check userid
    ${file_data}=          Load Json from file       ${jsonFile}
    ${json_obj}=           Update Value to Json      ${file_data}                     $..id._is_null    ${false}
    ${new_obj}=            Convert JSON to String    ${json_obj}
    Create File            userIdFalse.json          ${new_obj}
    Create Session         userIdFalse               ${baseURL}
    ${headers}             Create Dictionary         Content-Type=application/json
    ${newFileData}         Get Binary File           userIdFalse.json 
    ${response}            POST On Session           userIdFalse                      /graphql          data=${new_file_data}    headers=${headers}
    ${userIdfalse}         Set Variable              ${response.json()}
    Log                    ${userIdfalse}
    ${userId}              Get value From Json       ${userIdfalse}                   $..id
    Set Global Variable    ${userId}

Verify The user id is not empty
    Should Not Be Empty    ${userId}




